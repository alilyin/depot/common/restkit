import  Foundation

public protocol RESTClient: class {
    func get(_ url: URL) throws -> Data
}

public final class RestKit {
    
    public init(_ configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    private var session: URLSession
}

extension RestKit: RESTClient {
    
    public func get(_ url: URL) throws -> Data {
        var data: Data?
        var err: Error?
        let semaphore: DispatchSemaphore = DispatchSemaphore(value: 0)
        self.session.dataTask(with: url) { (d, r, e) in
            data = d
            err = e
            semaphore.signal()
        }.resume()
        semaphore.wait()
        if let error = err { throw NSError.error(code: (error as NSError).code, description: error.localizedDescription) }
        guard  let response: Data = data else { throw NSError.error(code: 0, description: "Empty response") }
        return response
    }
}

extension NSError {
    
    fileprivate static func error(code: Int, description: String, _ file: String = #file, _ line: Int = #line) -> Error {
        return NSError(domain: "com.alilyin.restkit", code: code, userInfo: [NSLocalizedDescriptionKey: "\(description)", NSFilePathErrorKey: "\(URL(fileURLWithPath: file).lastPathComponent):\(line)"])
    }
}
