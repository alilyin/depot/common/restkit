import XCTest
@testable import RestKit

final class RestKitTests: XCTestCase {
    
    private var config: URLSessionConfiguration!
    private var client: RESTClient!
    
    override func setUpWithError() throws {
        self.config = URLSessionConfiguration.default
        self.config.requestCachePolicy = .reloadIgnoringCacheData
        self.client = RestKit(self.config)
    }
    
    override func tearDownWithError() throws {
        self.config = nil
        self.client = nil
    }
    
    func testGet() {
        XCTAssertNoThrow(try self.client.get(URL(string: "https://google.com")!))
    }
    
    func testGetError() {
        XCTAssertThrowsError(try self.client.get(URL(string: "https://gopoopogle.qdsdasd")!))
    }

    static var allTests = [
        ("testGet", testGet),
        ("testGetError", testGetError),
    ]
}
